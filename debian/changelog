gnuchess-book (1.02-2.2) unstable; urgency=medium

  * Non-maintainer upload.

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

  [ Boyuan Yang ]
  * debian/control: User HTTPS for Homepage field.

 -- Boyuan Yang <byang@debian.org>  Sat, 14 Dec 2024 22:37:29 -0500

gnuchess-book (1.02-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 20 May 2022 23:09:00 +0200

gnuchess-book (1.02-2) unstable; urgency=medium

  * gnuchess-book must be built with gnuchess >= 6.2.2-2 (Closes: #834670)
  * Set myself as Maintainer instead of Oliver Korff <ok@xynyx.de>
    (Closes: #835323)
  * Move gnuchess from Depends to Suggests (Closes: #729164)
  * Use https for Vcs-Git and Vcs-Browser
  * Standards-Version 3.9.8

 -- Vincent Legout <vlegout@debian.org>  Fri, 02 Sep 2016 13:04:25 +0200

gnuchess-book (1.02-1) unstable; urgency=low

  * New upstream release
  * New maintainers: Oliver Korff and Vincent Legout
  * Use 3.0 (quilt) format
  * Update debhelper compatibility to 9
    - Update debian/compat and debhelper Build-Depends
  * debian/rules: Use dh
    - Use gnuchess new --addbook option to build the book
    - Add debian/clean and debian/gnuchess.install
    - Add get-orig-source target
  * debian/control:
    - Add ${misc:Depends}
    - Standards-Version 3.9.3 (No changes needed)
    - Depends on gnuchess (>= 6.0.2)
    - Update Vcs-* fields
  * README.Debian: the book file has been renamed book.bin
  * debian/copyright: update upstream location

 -- Vincent Legout <vlegout@debian.org>  Wed, 07 Mar 2012 19:44:47 +0100

gnuchess-book (1.01-2) unstable; urgency=low

  * Actually add watch file.

 -- Bradley Smith <bradsmith@debian.org>  Mon, 01 Jun 2009 01:50:50 +0100

gnuchess-book (1.01-1) unstable; urgency=low

  * New upstream release.
  * New maintainer. Closes: #503525.
  * Update compat version to 7 and upgrade debhelper depenency to (>= 7).
  * Update Standards-Version to 3.8.0. (No changes).
  * Add Homepage field.
  * Add Vcs-* fields.
  * Add watch file.

 -- Bradley Smith <bradsmith@debian.org>  Sun, 26 Oct 2008 20:17:23 +0000

gnuchess-book (1.00-2) unstable; urgency=low

  * Require bash for debian/rules, because we are using echo -e.
    (closes: #264430)

 -- Lukas Geyer <lukas@debian.org>  Wed, 11 Aug 2004 22:15:04 -0400

gnuchess-book (1.00-1) unstable; urgency=low

  * Initial release (closes: #163455)

 -- Lukas Geyer <lukas@debian.org>  Wed,  9 Oct 2002 19:53:27 -0400
